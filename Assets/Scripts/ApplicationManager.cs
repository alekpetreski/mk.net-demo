﻿using UnityEngine;
using UnityEngine.XR.WSA.Input;

public class ApplicationManager : MonoBehaviour
{
    public GameObject PrefabToGenerate;

    private const float MaxDistance = 10f;
    private GestureRecognizer _gestureRecognizer;

    private void Start()
    {
        InitializeGestureRecognizer();
    }

    private void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            PlacePrefab();
        }
    }

    private void InitializeGestureRecognizer()
    {
        _gestureRecognizer = new GestureRecognizer();
        _gestureRecognizer.SetRecognizableGestures(GestureSettings.Tap);
        _gestureRecognizer.Tapped += OnTap;
        _gestureRecognizer.StartCapturingGestures();
    }

    private void OnTap(TappedEventArgs obj)
    {
        PlacePrefab();
    }

    private void PlacePrefab()
    {
        RaycastHit hitInfo;
        var origin = Camera.main.transform.position;
        var direction = Camera.main.transform.forward;
        if (Physics.Raycast(origin, direction, out hitInfo, MaxDistance))
        {
            Instantiate(PrefabToGenerate, hitInfo.point, Quaternion.identity);
        }
    }
}